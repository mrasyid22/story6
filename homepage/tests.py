from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from homepage.views import homepage, redirecting
from homepage.forms import StatusForm
from homepage.models import Status

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time

# Create your tests here.


class UnitTest(TestCase):
    #Test views & urls
    def test_home_url_exist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist_is_not_exist(self):
        response = Client().get('/nothing/')
        self.assertEqual(response.status_code, 404)

    def test_home_using_homepage_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, 'homepage.html')
    
    def test_home_using_homepage_function(self):
        response = resolve('/home/')
        self.assertEqual(response.func, homepage)
    
    def test_home_is_completed(self):
        request = HttpRequest()
        response = homepage(request)
        response_html = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', response_html)
    
    def test_home_page_title_is_right(self):
        request = HttpRequest()
        response = homepage(request)
        response_html = response.content.decode('utf8')
        self.assertIn('<title>Story 6 Ocit</title>', response_html)
    
    #Test Models
    def setUp(cls):
        Status.objects.create(status="Lagi satnight nih")

    def test_if_models_in_database(self):
        objects = Status.objects.create(status="Galau nih")
        count_Object = Status.objects.all().count()
        self.assertEqual(count_Object, 2)

    def test_if_status_already_exist(self):
        statusData = Status.objects.get(id=1)
        status = statusData._meta.get_field('status').verbose_name
        self.assertEqual(status, 'status')

    #Test Redirect landing page to home
    def test_landing_page_using_redirecting_func(self):
        response = resolve('/')
        self.assertEqual(response.func, redirecting)
    
    def test_landing_page_redirecting(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
    
    def test_landing_page_redirect_home(self):
        response = Client().get('/')
        self.assertRedirects(response, '/home/')
    
    #Test Forms
    def test_forms_in_input_html(self):
        form = StatusForm()
        self.assertIn('id="id_status"', form.as_p())

    def test_forms_validation_blank(self):
        form = StatusForm(data={'status':''})
        self.assertFalse(form.is_valid())
        self.assertEquals(form.errors['status'], ['This field is required.'])
    
    def test_forms_in_template(self):
        request = HttpRequest()
        response = homepage(request)
        response_html = response.content.decode('utf8')
        self.assertIn('<form method="POST" class="post-form form-group"', response_html)

class FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_input(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/home/')
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('button')
        time.sleep(3)
        status.send_keys('TEST STATUS LAGI')
        time.sleep(3)
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn("Coba Bang", selenium.page_source)
